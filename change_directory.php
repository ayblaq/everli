<?php

class Path
{
    public $current_path = "";

    public function __construct($path)
    {
        $this->current_path = $path;
    }

    public function cd($path)
    {
        $dir_history = explode('/', $this->current_path);
        $cur_dirs = explode('/', $path);

        $updated_path = [];
        foreach ($cur_dirs as $cur_dir) {
            if ($cur_dir === '..') {
                array_pop($dir_history);
            } else {
                $updated_path[] = $cur_dir;
            }
        }

        $updated_path = array_merge($dir_history, $updated_path);

        $this->current_path = implode('/', $updated_path);
    }

    public function getWd(): string
    {
        return $this->current_path;
    }
}