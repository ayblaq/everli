
let store = {}

const fn = {
    convert_to_binary: (value) => {
        let inverse = fn.reverse_binary((value).toString(2))
        store[value] = inverse
        store[inverse] = value
    },
    reverse_binary: (binary) => {
        let reversed = binary.split('').reverse().join('');

        return parseInt(reversed, 2)
    },
    get_inverse: (value) => {
        let number = Math.abs(value)
        if (!store[number]) {
            fn.convert_to_binary(number)
        }

        return value < 0 ? - store[number] : store[number]
    }
}

console.log(fn.get_inverse(12))