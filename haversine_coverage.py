import numpy as np
from haversine import haversine, Unit
from sklearn.neighbors import BallTree, KDTree

locations = [
    {'id': 1000, 'zip_code': '37069', 'lat': 45.35, 'lng': 10.84},
    {'id': 1001, 'zip_code': '37121', 'lat': 45.44, 'lng': 10.99},
    {'id': 1001, 'zip_code': '37129', 'lat': 45.44, 'lng': 11.00},
    {'id': 1001, 'zip_code': '37133', 'lat': 45.43, 'lng': 11.02}
];

shoppers = [
    {'id': 'S1', 'lat': 45.46, 'lng': 11.03, 'enabled': True},
    {'id': 'S2', 'lat': 45.46, 'lng': 10.12, 'enabled': True},
    {'id': 'S3', 'lat': 45.34, 'lng': 10.81, 'enabled': True},
    {'id': 'S4', 'lat': 45.76, 'lng': 10.57, 'enabled': True},
    {'id': 'S5', 'lat': 45.34, 'lng': 10.63, 'enabled': True},
    {'id': 'S6', 'lat': 45.42, 'lng': 10.81, 'enabled': True},
    {'id': 'S7', 'lat': 45.34, 'lng': 10.94, 'enabled': True},
];


def _convert_to_rad(data):
    rad_data = []
    for row in data:
        rad = np.deg2rad([row['lat'], row['lng']])
        rad_data.append(rad)

    return rad_data


def _calculate_coverage(shoppers, locations):
    tree = _build_ball_tree(locations)
    num_location = len(locations)

    coverage_area = []
    for shopper in shoppers:
        rad = np.deg2rad([shopper['lat'], shopper['lng']])
        res_coverage = tree.query_radius(rad.reshape(1, -1), r=max_coverage_distance)
        num_neighbors = len(res_coverage[0])
        coverage_area.append(
            {'shopper_id': shopper['id'], 'coverage': _compute_percentage(num_neighbors, num_location)})

    # sort coverage area by percentage coverage
    sorted_list = sorted(coverage_area, key=lambda d: d['coverage'], reverse=desc_order)

    return sorted_list


# calculate coverage percentage
def _compute_percentage(num_neighbor, num_location):
    try:
        percentage = 100 - (abs(num_location - num_neighbor) / num_location * 100)
    except ZeroDivisionError:
        percentage = 0
    return percentage


# convert location to radians and build balltree based on haversine distance
def _build_ball_tree(data):
    data_rad = _convert_to_rad(data)

    return BallTree(data_rad, metric='haversine')


if __name__ == "__main__":
    # 10km is converted to radians.
    max_coverage_distance = 10 / 6371
    desc_order = True

    print(_calculate_coverage(shoppers, locations))
